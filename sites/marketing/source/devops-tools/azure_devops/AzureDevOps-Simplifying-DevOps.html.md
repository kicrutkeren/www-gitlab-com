---
layout: markdown_page
title: "Azure DevOps for Simpliying DevOps"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->



**Azure DevOps** does not provide built-in App Performance Monitoring (APM). Although Microsoft has Azure Monitoring, it is not integrated with Azure DevOps. 

**GitLab** provides monitoring built-in.
   
