---
layout: markdown_page
title: "Azure DevOps for the Business Decision Maker"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}


## Azure DevOps Strengths

* Single pre-integrated solutions for DevOps are starting to resonate.  Multiple data points indicate that the market is starting to perceive Microsoft And GitLab as the top two end to end DevOps solutions today.  Azure DevOps is viewed as a unified suite with a single user interface

* Microsoft has strong enterprise relationships and multi-year ELAs.  ELAs make it easy for customers to pick and choose the products they need with minimal friction from the “buffet” of Microsoft choices under the ELA. Microsoft products only have to be “good enough” for the convenience that ELAs offer. Azure DevOps users are happy with Azure DevOps and see it as a single app for most of their flow/work - integrated “enough” for what they need to do now.

* GitLab lacks Manual Testing Management capabilities, while Azure DevOps provides Azure Test Plans built-in for a separate licensing fee.

* Azure DevOps MarketPlace and GitHub MarketPlace are key differentiators for Microsoft in terms of providing extensibility to their platforms.  Customers want the  ability to extend Azure DevOps/GitHub and to integrate with their existing tools.

* Microsoft has a strong partner ecosystem in place today to enable a broader reach and adoption of its offerings. 

* Microsoft’s breadth remains both its key strength and weakness.  Microsoft’s strength is the wide range of Office productivity tools, developer tools, and enterprise IT management solutions that are pervasive in many enterprises. 

## Azure DevOps Limitations and Challenges

* Microsoft dev tool customers are not universally positive about Azure DevOps and/or GitHub.  Those customers who _have chosen to move to Azure DevOps and/or GitHub _generally like the Azure DevOps / GitHub tools.  However, the larger Microsoft tool customer base (legacy TFS/VSTS and others) are not too eager to be pushed into Azure DevOps and/or GitHub.

* Microsoft does a good job of being extensible through their Marketplace for plugins although the Marketplace offerings are similar to Jenkins plugins (not all MS owned or guaranteed, many not rated and of questionable reliability). Under the covers, the complexity of multi-app shows when using Azure DevOps with plugins.  Plugins can be costly to maintain and support.

* Microsoft’s breadth is also a weakness driven by inherent corporate and organizational complexity (multiple competing business units within the same structure).  This complexity creates multiple product and pricing/licensing options, product group competition and misalignment in business goals.

* Microsoft customers still do not believe they are a cloud “neutral vendor”, believing that working with Microsoft predisposes them to being locked in with a single vendor. This is despite Microsoft itself appearing to have genuinely embraced a vendor-neutral, run anywhere philosophy (example - ability to work with K8, etc.).  For example, their messaging states - “Continuously build, test, and deploy to any platform and cloud.”

## Azure DevOps Selling Strategy

* Microsoft’s target TAM for developer tools investments extends beyond the immediate DevOps tools TAM.  Microsoft’s DevOps solution offering is a means to grow their market share in the Cloud TAM which is approximately $69b. This means Microsoft will be willing to sacrifice DevOps tools in order to win Cloud business.

* Microsoft is working to build stickiness around long term dependencies with Azure services**, in particular higher-value services like translation, image recognition, etc.  

* Microsoft has a strong partner ecosystem** for reaching a broader market and appropriate buyers (management/IT consulting and technology partners) and offering a complete solution for successful implementation (professional services).   


## GitLab Differentiators

* **Auto DevOps**: Want an automatic devops pipeline that takes your code into production without having to configure or maintain anything?  Azure DevOps has nothing like GitLab Auto DevOps. They have pipeline templates, but nothing that runs out of the box with such complete functionality.

* **Canary and Incremental Role Outs**: Azure DevOps does not have any out of the box release methodologies built in like GitLab does (granted, with GitLab, you need to be using Kubernetes to get these capabilities)

* **No vendor lock-in**: Microsoft claims cloud-neutral, but will always make it easiest with Azure first.  They expects the majority of their revenue to come from selling cloud compute. They might claim cloud-neutral, but once you are on their platform they will do everything they can to get you to stay there. They’ve shown this behavior time and time again.  GitLab is independent of any cloud provider and is truly cloud-neutral.

* **Application Security scanning**  Microsoft has no built-in tools to do SAST, DAST, Container scanning, dependency scanning, or Open Source license compliance scanning, while GitLab offers extensive built-in application security scanning.

* **Open Core**:  GitLab is [open core](https://about.gitlab.com/2016/07/20/gitlab-is-open-core-github-is-closed-source/) and anyone can contribute changes directly to the codebase, which once merged would be automatically tested and maintained with every change.

## GitLab vs Azure DevOps Innovation Pace

* Microsoft has accelerated its rate of innovation.  Azure DevOps and GitHub are now releasing quicker than GitLab. 
    * Evidence can be seen by an analysis of the [2019 Azure DevOps Services release notes](https://docs.microsoft.com/en-us/azure/devops/release-notes/2019/sprint-156-update) (showing 12 releases in 7 months - 2019-01-14 to 2019-08-12) and release cadence acceleration from a constant 4 to 3 weeks. The [GitHub changelog](https://github.blog/changelog/) also shows us that the GitHub team also has a high release cadence, releasing features on a 1-2 day cadence. => Sid: Can we count new features velocity instead of release frequency?

    * Their project manager shared that they are releasing on a 3-4 week pace. This seems evident based on their [published roadmap](https://docs.microsoft.com/en-us/azure/devops/release-notes/). The same project manager also shared that Azure DevOps Server (TFS) will be 3-4 months behind on adopting new features (also evident by their published roadmap). They are both from the same code base.
