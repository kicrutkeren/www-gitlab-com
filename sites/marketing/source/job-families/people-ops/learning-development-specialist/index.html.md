---
layout: job_family_page
title: "Learning & Development"
---

The Learning and Development (L&D) job family is responsible for building and maintaining a learning culture at GitLab. Specifically, L&D plays a strategic role in the organization to ensure team members acquire and build skills, knowledge, and competencies that will enable them to perform at a high level. The goal of the job family is to develop and change the behavior of team members and groups to reach organizational objectives and results. 

## Learning & Development Associate 

The Learning & Development Associate will support the identification and design of learning solutions to support GitLab's growth. They will be responsible for maintaining the back-end management of learning administration. They will have a passion for supporting team members growth and development while helping to build a learning culture at GitLab. 

### Responsibilities

- Maintains the back-end management of learning administration to include certifications, knowledge assessments, external learning inquires, and internal training programs
- Enhance and build out our Handbook resources for learning
- Develops Learning Management System (LMS) adoption guides for team members and enablement activities with guidance from the L&D Partner 
- Supports the L&D Generalist with integrating the LMS into GitLab. Utilizes the LMS course authoring tool to create Handbook first course content
- Identify, develop, and deliver programs that support the growth of our team members and GitLab
- Develop customized learning paths for team members that incorporate customized and curated learning content
- Supports the marketing strategy and develops communication material to promote internal training
- Works with the L&D Generalist and Partner to develop a comprehensive GitLab learning curriculum that incorporates multi-modality learning formats (bite-sized training, virtual instructor-led, hands-on workshops, self-study, etc) while ensuring all material is Handbook first
- Track and monitor training consumptions through reporting and analytics
- Solicit and incorporate team member feedback into our course content and learning experience

### Requirements

- Ability to use GitLab
- Strong interpersonal and communication skills
- Comfortable delivering training to various functional groups and audience sizes
- Ability to bring innovative Learning & Development ideas and proposals to the team that can improve team member career development
- Self-starter, ability to learn fast and contribute
- Interest in developing skills in adult learning theory and instructional design while applying those lessons to new learning content at GitLab
- Strong organizational knowledge and understanding of the current gaps within career development and L&D at GitLab
- Passion for career development, training, leadership, and helping team members develop to their fullest potential
- Nice to have: experience with instructional design and developing learning content

### Job Grade

The Learning & Development Associate is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades). 

### Performance Indicators

- [Engagement Survey Growth and Development Score](/handbook/people-group/learning-and-development/#engagement-survey-growth-and-development-score--x)
- [Rate of internal job promotions](/handbook/people-group/learning-and-development/#rate-of-internal-job-promotions--x)
- [12 month voluntary team member turnover related to growth](/handbook/people-group/learning-and-development/#12-month-voluntary-team-member-turnover-related-to-growth--x)

### Career Ladder

The next step in the Learning and Development job family is to move into the Learning & Development Generalist role. 

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/). 

* 30-minute interview with L&D Partner 
* 30-minute interview with L&D Generalist
* 30-minute interview with a People Business Partner (PBP)
* A final assessment, candidates may be invited to give a short 10-minute presentation on managing underperformance to the L&D team

## Learning & Development Generalist

### Job Grade

The Learning & Development Generalist is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Support the L&D Partner to identify, develop, and deliver programs that support the growth of our team members and GitLab
- Work with the L&D Partner to roll out an internal learning and development roadmap for all GitLab managers and individual contributors
- Research, develop, and facilitate exciting and impactful asynchronous and synchronous L&D programs
- Coordinate an effective marketing strategy to promote internal training
- Contribute to the course catalog which will include e-learning, instructor-led courses, and hands-on workshops, utilizing the GitLab handbook to ensure the trainings are accessible to everyone
- Own the L&D metrics and provide feedback to the L&D Partner to iterate upon programs and courses for continuous improvement
- Solicit and incorporate team member feedback into our course content and experience securely

### Performance Indicators
- [Engagement Survey Growth and Development Score](/handbook/people-group/learning-and-development/#engagement-survey-growth-and-development-score--x)
- [Rate of internal job promotions](/handbook/people-group/learning-and-development/#rate-of-internal-job-promotions--x)
- [12 month voluntary team member turnover related to growth](/handbook/people-group/learning-and-development/#12-month-voluntary-team-member-turnover-related-to-growth--x)

## Learning and Development Partner

The Learning & Development Partner will identify and design the right learning solutions to support GitLab's growth. They will have deep experience in assessing organizational needs and developing a variety of learning solutions to drive development and growth within the company. They will also build and scale initiatives that focuses on company culture, individual development, strengthening our leadership, and organizational learning.

### Job Grade

The Learning & Development Partner is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Partner with members of the People group and the leadership team to identify, develop, and deliver solutions that support the growth of our team members and GitLab's success
- Establish an internal learning and development roadmap for all GitLab managers and individual contributors
- Research, develop, and facilitate exciting and impactful asynchronous and synchronous L&D programs
- Iterate on existing materials and design and develop new L&D content, utilizing [GitLab's YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) and [handbook](https://about.gitlab.com/handbook/handbook-usage/)
- Working [handbook first](https://about.gitlab.com/handbook/handbook-usage/#why-handbook-first)
- Collaborate with the Sales team to design and deliver orientation content that develops new hires' understanding of GitLab’s business and platform
- Select a learning platform for GitLab that incorporates content from the handbook and provides methods for tracking assessments and completion
- Coordinate an effective marketing strategy to promote internal training
- Create the course catalog which will include e-learning, instructor-led courses, self study (books and articles), and hands-on workshops, utilizing the GitLab handbook to ensure learning solutions are accessible to everyone
- Monitor L&D metrics and iterate upon programs and courses for continuous improvement
- Create and design the supporting course material for all development programs, both for instructor-led and e-learning
- Solicit and incorporate team member feedback into our course content and experience

### Requirements

- 5+ years experience in related work such as instructional design and developing learning content
- Track record of designing engaging and impactful development programs that improves individual, team, and company performance
- Exceptional written and interpersonal skills
- Experience developing self-service learning content, such as e-learning modules
- Experience designing and delivering webinars or synchronous online courses
- Strong logistical planning and organizational skills
- Team-orientated, engaging, and energetic
- Capable of working collaboratively across multiple departments
- Passionate about personal development, training, learning, and seeing individuals develop to their fullest potential
- The ability to thrive in a fast-paced environment
- You share our [values](/handbook/values), and work in accordance with those values
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
- Ability to use GitLab

### Performance Indicators
- [Engagement Survey Growth and Development Score](/handbook/people-group/learning-and-development/#engagement-survey-growth-and-development-score--x)
- [Rate of internal job promotions](/handbook/people-group/learning-and-development/#rate-of-internal-job-promotions--x)
- [12 month voluntary team member turnover related to growth](/handbook/people-group/learning-and-development/#12-month-voluntary-team-member-turnover-related-to-growth--x)

### Hiring Process

* Qualified candidates will be invited to schedule a 30 minute screening call with one of our Recruiters.
* Next, candidates will be invited to schedule a 30 minute interview with our Director of People Operations.
* Next, the candidate will be invited to interview with a member of the L&D team, a member from our Sales Enablement team, a People Business Partner and our Internal Strategy Consultant.
* After that, our CEO may choose to conduct a final interview.
* As a final assessment, candidates will be required to prepare and present a short 15 minute training on giving feedback, to the Director of People Operations

### Career Ladder

The next step in the Learning and Development job family is to move into management or leadership which is not yet defined at GitLab. Currently, the Learning & Development reports to the [People Leadership](/job-families/people-ops/people-leadership/) job family. 
