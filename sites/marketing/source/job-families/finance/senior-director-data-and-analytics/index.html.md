---
layout: job_family_page
title: "Director of Business Operations"
---

## Director, Business Operations

### Job Grade

The Director, Business Operations is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Drive the scope and effectiveness of the [Business Operations](/handbook/business-ops/#customer-lifecycle) function at GitLab.
* Ensure the Company’s cloud and on-premise data is centralized into a single data warehouse that can support data analysis requirements from all functional groups of the Company.
* Create a common data framework so that all company data can be analyzed in a unified manner.
* Work with the product and executive management teams to create a data enabled user journey.
* Design, implement, and manage the organization's go-to-market systems architecture, including the sales and marketing infrastructure across Marketing, Sales, Customer Success and Accounting used to support the customer journey.
* Create and execute a plan to develop and mature our ablility to measure and optimize usage growth and our [user journey](/handbook/journeys/#user-journey).
* Ensure that all transactional systems can communicate with each other either directly or via the data warehouse and that production data adheres to a unified data model.
* Ensure that each metric in the Company’s dashboard has a single source of Truth and that data ownership and validation are incorporated on a consistent basis.
* Determine the level of integration necessary between transactional systems to deliver the right data in the right context to users.
* Develop a roadmap for system expansion, evaluate existing systems and ensure future systems are aligned with the Company’s data architecture plan which you will largely help develop.
* Implement a set of processes that ensure any changes in transactional system architecture are documented and their impact on the company’s overall data integrity are considered prior to changes being made.
* Collaborate with all functions of the company to ensure data needs are addressed and system.
* Hold regular 1:1’s with all direct reports
* This position reports directly to the CFO and works closely with the executive team to develop an organization plan that addresses company wide analytic resources in either a direct report or matrix model.
* This position will manage the data analytics, data engineering and system administration (Including salesforce, marketo, zendesk, zuora) functions.

### Requirements

* Postgraduate work Masters or PhD in a quantitative field such as math, physics, computer science, statistics etc.
* Minimum 4 years hands on experience in a data analytics role.
* Experience with a high growth company using on-premise tools and on-demand (SaaS) transactional systems.
* Hands on experience with Python, MySQL, SQL and relational databases.  Experience with Snowflake is a plus.
* Have previously lead a corporate data platform project.
* Experience with open source data warehouse tools.
* Experience working with multiple executive level business stake holders.
* Must have experience with analytic and data visualization tools such as Looker.
* Must have experience with Salesforce, Zuora, Zendesk and Marketo.
* Share and work in accordance with our [values](/handbook/values/).
* Must be able to work in alignment with Americas timezones.
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)
* Ability to use GitLab


### Performance Indicators (PI)

*  [New Hire Location Factor < 0.69](/handbook/business-ops/metrics/#new-hire-location-factor--069)
*  [% of team who self-classify as diverse](/handbook/business-ops/metrics/#percent--of-team-who-self-classify-as-diverse)
*  [Discretionary bonus per employee per month > 0.1](/handbook/business-ops/metrics/#discretionary-bonus-per-employee-per-month--01)
*  [Cost Actual vs Plan](/handbook/business-ops/metrics/#cost-actual-vs-plan)
*  [System roll out vs plan](/handbook/business-ops/metrics/#system-roll-out-vs-plan)


### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our CFO
* Candidates will then be invited to schedule a second interview with our CRO, Sr. Director of Demand Generation
* Finally, candidates will interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).

### Career Ladder 

The career ladder for the Director, Business Operations is not yet defined at this time. 
