---
title: "How GitLab Pages uses the GitLab API to serve content"
author: "Jaime Martínez"
author_gitlab: jaime
author_twitter: jimbart098
categories: unfiltered
image_title: '/images/blogimages/retrosupply-jLwVAUtLOAQ-unsplash.jpg'
description: "GitLab Pages is changing the way it reads a project's configuration to speed up booting times and slowly remove its dependency to NFS"
tags: GitLab, gitlab-pages, engineering, cloud, kubernetes
guest: false
ee_cta: false # required only if you do not want to display the EE-trial banner
install_cta: false # required only if you do not want to display the 'Install GitLab' banner
twitter_text: 'How @GitLab Pages uses the GitLab API to serve content'
postType: product # i.e.: content marketing, product, corporate
merch_banner: merch_one
merch_sidebar: merch_one
---

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
This blog post is [Unfiltered](/handbook/marketing/blog/unfiltered/#legal-disclaimer)
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

[GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) allows you to create and host GitLab project websites from a user account or group for free on [GitLab.com][gitlab.com] or on your self-managed GitLab instance.

In this post, I will explain how the [GitLab Pages daemon][pages-daemon] obtains a domain's configuration using the 
GitLab API, specifically on [GitLab.com][gitlab.com].

## How does GitLab Pages know where to find your website files?

GitLab Pages is moving to using object storage to store the contents of your web site. You can follow the development of this new feature [here][object-storage-epic].

At the time of writing, GitLab Pages uses an NFS shared mount drive to store the contents of your website.
You can define the value of this path by defining the [`pages_path`](https://docs.gitlab.com/ee/administration/pages/#change-storage-path) in your `/etc/gitlab/gitlab.rb` file.

When you deploy a website using the `pages:` keyword in your `.gitlab-ci.yml` file, a `public` path artifact must be defined, containing the files available for your website. This `public` artifact eventually makes its way into the NFS shared mount.

When you deploy a website to GitLab Pages a domain will be created based on the [custom Pages domain you have configured](https://docs.gitlab.com/ee/administration/pages/#configuration). For [GitLab.com][gitlab.com], the pages domain is `*.gitlab.io`, if you create a project named `myproject.gitlab.io` and enable HTTPS, a wildcard SSL certificate will be used.
You can also [setup a custom domain][custom-domains] for your project, for example `myawesomedomain.com`.

For every project (a.k.a. domain) that is served by the Pages daemon, there must exist a directory in the NFS shared mount that matches your domain name and holds its contents. For example, if we had a project named `myproject.gitlab.io`, the Pages daemon would look for your `.html` files under `/path/to/shared/pages/myproject/myproject.gitlab.io/public` directory.
This is how GitLab Pages serves the content published by the `pages:` keyword in your CI configuration.

Before GitLab 12.10 was released on [GitLab.com][gitlab.com], the Pages daemon would rely on a file named `config.json` located in your project's directory in the NFS shared mount, that is `/path/to/shared/pages/myproject/myproject.gitlab.io/config.json`.
This file contains metadata related to your project and [custom domain names][custom-domains] you may have setup.

```json
{
  "domains":[
    {
      "Domain":"myproject.gitlab.io"
    },
    {
      "Domain": "mycustomdomain.com",
      "Certificate": "--certificate contents--",
      "Key": "--key contents--"
    }
  ],
  "id":123,
  "access_control":true,
  "https_only":true
}
```
GitLab Pages has been a very popular addition to GitLab, and over time the number of hosted websites on [GitLab.com][gitlab.com] has increased a lot.
On start-up, the Pages daemon would [traverse all directories](https://gitlab.com/gitlab-org/gitlab-pages/-/blob/v1.21.0/app.go#L448) in the NFS shared mount and load the configuration of all the deployed Pages projects into memory.
At some point in time, the Pages daemon would take [over 20 minutes to load](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/252) per instance on [GitLab.com][gitlab.com]!

## GitLab API-based configuration

> [Introduced](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/282) in GitLab 12.10

On [GitLab.com][gitlab.com], the Pages daemon now sources the domain configuration via an internal API endpoint
`/api/v4/internal/pages?domain=myproject.gitlab.io`.
This is done on demand per domain and the configuration is cached in memory for a certain period of time
to speed up serving content from that Pages node.

The response from the API is very similar to the contents of the `config.json` file:

```json
{
    "certificate": "--cert-contents--",
    "key": "--key-contents--",
    "lookup_paths": [
        {
            "access_control": true,
            "https_only": true,
            "prefix": "/",
            "project_id": 123,
            "source": {
                "path": "myproject/myproject.gitlab.io/public/",
                "type": "file"
            }
        }
    ]
}
```

You can see that the source type is `file`. This means that the Pages daemon will still serve the contents from the NFS shared mount. We are actively working on removing the NFS dependency from GitLab Pages by [updating the GitLab Pages architecture](https://gitlab.com/groups/gitlab-org/-/epics/1316).

We are planning to [transition GitLab pages to object storage instead of NFS][object-storage-epic]. This will essentially [enable GitLab Pages to run on Kubernetes](https://gitlab.com/gitlab-org/gitlab/-/issues/39586) in the future.

## Self-managed GitLab instances

The changes to the GitLab Pages architecture were piloted on [GitLab.com][gitlab.com], which is possibly the largest GitLab Pages implementation.
Once all the changes supporting the move to an API-based configuration are completed, they will be rolled out to self-managed customers.
You can find more details and the issues we faced while rolling out API-based configuration in this [issue](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/282).

If you can't wait to speed up your Pages nodes startup, we have a potential guide in this [issue description](https://gitlab.com/gitlab-org/gitlab/-/issues/28298#potential-workaround) which explains how we enabled the API on [GitLab.com][gitlab.com]. However, this method will be removed in the near future.

## Domain source configuration and API status

In the meantime, we are working towards adding [a new configuration flag for GitLab Pages](https://gitlab.com/gitlab-org/gitlab/-/issues/217912) which will allow you to choose the domain configuration source by specifying `domain_config_source` in your `/etc/gitlab/gitlab.rb` file.
By default, GitLab Pages will use the `disk` source configuration the same way is used today.

In the background, the Pages daemon will start [checking the API status](https://gitlab.com/gitlab-org/gitlab-pages/-/merge_requests/304) by calling the `/api/v4/internal/pages/status` endpoint. This will help you check if the Pages daemon is ready to talk to the GitLab API, especially when you are [running Pages on a separate server](https://docs.gitlab.com/ee/administration/pages/#running-gitlab-pages-on-a-separate-server).

Please check the [GitLab Pages adminstration guide](https://docs.gitlab.com/ee/administration/pages/#troubleshooting) for further troubleshooting.

<!-- References -->
[gitlab.com]: https://gitlab.com "GitLab.com"
[pages-daemon]: https://gitlab.com/gitlab-org/gitlab-pages "GitLab Pages daemon"
[go]: https://golang.org "Go"
[custom-domains]: https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/ "Custom domains and SSL/TLS Certificates"
[object-storage-epic]: https://gitlab.com/groups/gitlab-org/-/epics/3901 "Transition to Object Storage from NFS"

<!-- image: image-url -->
Cover image by [@RetroSupply](https://unsplash.com/@retrosupply) on [Unsplash](https://unsplash.com/photos/jLwVAUtLOAQ)
{: .note}