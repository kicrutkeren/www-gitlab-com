---
layout: handbook-page-toc
title: "Biggest Tailwinds"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

We believe that the market opportunity for a complete DevSecOps platform designed as a single application for the software development lifecycle is [several billion dollars and expanding](https://about.gitlab.com/handbook/sales/tam/#:~:text=Total%20Addressable%20Market%20(TAM)%20is,revenue%20per%20user%20(ARPU).&text=Additional%20potential%20users%3A,Product%20managers). There are three primary trends outlined below that we have identified as the most significant to supporting the long term success of our business.
We also have a [biggest risks page](/handbook/leadership/biggest-risks/).
## [1. Digital Transformation](/blog/2019/03/19/reduce-cycle-time-digital-transformation/)
[Customer Experience](https://docs.gitlab.com/ee/ci/review_apps/index.html#visual-reviews-starter)

[Software is Eating the World](https://a16z.com/2011/08/20/why-software-is-eating-the-world/)

## [2. DevOps](/devops)
[Developers as the New Kingmakers](https://dzone.com/articles/developers-are-the-new-kingmakers)

[DevOps Tooling Consolidation](https://devops.com/challenges-devops-standardization/)

[Lack of Developers](https://stackoverflow.blog/2017/03/09/developer-hiring-trends-2017/)

[Open Source](/20-years-open-source/)

## [3. Multicloud](https://about.gitlab.com/topics/multicloud/)
[Cloud Native and the Adoption of Kubernetes](/cloud-native/)

[Microservices](/topics/microservices/)

[Observability](https://siliconangle.com/2019/09/30/quickening-race-lead-cloud-native-computing-observability/)

[Progressive Delivery](/blog/2019/04/19/progressive-delivery-using-review-apps/)

[Workloads Moving to the Cloud](https://www.synopsys.com/blogs/software-security/cloud-migration-business/)
