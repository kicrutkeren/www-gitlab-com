---
layout: handbook-page-toc
title: Support Team Member Time Off
---

## Support Team Member Time Off
{: .no_toc}
The Support Team follows [GitLab's paid time off policy](/handbook/paid-time-off).
And we balance that with the need to deliver support to our customers every day.
This page is intended to provide all Support Team Members with an understanding
of what we need to do to achieve that balance, making it possible for all to
take time off as needed and desired while we as a team continue to deliver
amazing support and service to our customers.

There's a special point that needs to be emphasized, and that is that time off
for any team member only serves its purpose if that person truly takes the time
off. Nobody should feel pressure or the need to check Slack, email, or anything
else associated with work during their time off. **Especially when you are taking
time to be on holiday or to be with family or friends, please, in the timeless
words of the snow queen, Elsa, "let it go."**

### Responsibilities
When you are approaching a planned absence from work, it is your responsibility
to take reasonable actions to prepare the team and your customers for your
absence. By making these preparations, you will also be making it easier for
yourself to disconnect from work and make your time off effective.

#### Choosing time off
A little thoughtfulness on everybody's part will go a long way toward making it
possible for everyone to take their desired days off. As you look to plan your
time off, please:

- consider a different time off schedule if the
  [**Support - Time Off**](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com)
  calendar shows that on any of the days you would be absent the regional team
  would be short by 20% or more of its people 
- schedule your time off as far in advance as you can, especially for days that
  may be popular or that are particularly important to you
- don't lock yourself into nonrefundable travel itineraries before you've taken
  steps such as the above to be sure you can get the planned days off
- if possible, plan your time off to avoid disruptions to the various rotations
  such as on-call

#### One-time actions
There are a few things you will need to do just once, before you take any time off.

1. Be sure you have access to the
[**Support - Time Off**](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com)
team calendar.

   You should have been granted access when you joined Support. But
if you don't have it, please ask for help in the `#support_team_chat` channel
on Slack and someone will share the calendar with you.

1. Configure PTO Ninja to link to both your personal calendar and the
'Support - Time Off' calendar
   1. In Slack, click the `+` sign next to 'Apps' at the bottom of the left sidebar
   1. Search for 'PTO Ninja' and click 'View'
   1. Under 'Home', click on 'Connect your calendar'
   1. Click on 'Allow' and complete the actions to sync your calendar to PTO Ninja
      - You will see a 'Success! Your calendar has been connected.' message and
        your calendar listed under 'Your synced calendar' in PTO Ninja on Slack
      - ***NOTE: this also serves to block your availability in Calendly***
   1. After your personal calendar is linked, click 'Add calendar' under
   'Additional calendars to include?'. The 'Support - Time Off' calendar ID is
   `gitlab.com_as6a088eo3mrvbo57n5kddmgdg@group.calendar.google.com`

#### Taking off less than half a day

- please do so without hesitation -- go for a walk or run, grab a coffee, eat
lunch, go to an appointment, run an errand, or just plain take a brain break
- please do **not** add time-off events to the shared calendar or via PTO Ninja
- please **do** consider blocking off your own calendar so that customer calls or
other meetings won't be scheduled during your time away.
- Consider updating your Slack status to reflect you're away.

#### Taking off half a day or more
For planned absences of half a day or more, you will need to take various steps
to communicate your absence, to reschedule planned events, to ensure continued
service to customers, and to arrange coverage for your assignments such as being
on call. The exact steps vary somewhat depending on the relative length of your
absence.

##### Start with these steps for all absences of at least half a day

1. Use PTO Ninja to schedule the time
   PTO Ninja uses your calendar links to block off time in your Google Calendar,
   which has the cascading effect that Calendly will also show you as busy. As a
   result:
   - Google Calendar invitations will be automatically declined
   - Calendly will not allow you to be assigned to Customer Calls
1. If you are assigned to any PagerDuty shifts (such as on-call) during the time
   you are taking off, arrange for coverage:
   1. Find a volunteer to cover for you; get help from your manager if necessary
   1. Create the appropriate override in PagerDuty
1. Reschedule or cancel any impacted meetings in which you are a key participant

##### Then, for absences of one or two days

1. Check with your customers
   For any tickets that will still be open and active during your absence, and
   that might require an update during that time, ask the customer whether they
   would prefer to pause the ticket till your return or have someone else step
   in to work with them. If they want to pause, put the ticket on hold.
1. Groom your queue
   For each ticket that won't be on hold, create an internal comment summarizing
   the status and plan. Specifically, be sure to include:
   - A concise statement of the problem or request
   - A brief explanation of what's been done so far - what's been tried, what
     progress has been made
   - The current status - what remains to be done
   - The plan - expected next steps and the reasons behind them

##### Or, for absences of three or more days

1. Notify the team of your upcoming absence
   Add an item in the SWIR document the Friday before your absence to announce
   your time off.
1. Change your workflow two or three days in advance of your absence:
   1. stop taking new tickets
   1. work to resolve and close as many of your assigned tickets as possible
   1. contribute more to other people's tickets - internal comments, pairing
      sessions - to make up for not taking new tickets of your own
1. Find homes for your remaining tickets
   On your last day, please inform your users that your tickets will be
   reassigned due to your upcoming absence. Then, find someone willing to take
   over each ticket, and if possible review the tickets with them before
   reassigning. If you can't find someone to take a ticket, you can 'unassign'
   by assigning it to a 'group' (e.g. 'Support EMEA', 'Support APAC',
   'Support AMER'). If the user has specified a preferred region, select the 
   corresponding group. Otherwise, select any group - the ticket will still be
   visible for all Support Engineers. Be sure in this situation to follow the
   Groom your queue process described above so that the next engineer can come
   up to speed very quickly.

### Tips

In addition to the companywide tips in
[Communicating Your Time Off](/handbook/paid-time-off/#communicating-your-time-off),
take a look at these tips to increase your ability to disconnect from work,
truly make the most of your time off, and make it easier to return to work:

1. Unsubscribe from any MRs that will likely resolve while you're away, and then
   you won't have to deal with all the emails from them when you return.
1. Set up some email filters to move emails into buckets that you can address
   one-at-a-time when you return.
1. Disconnect yourself from Slack and Gmail in any of these ways:
   *NOTE: PTO Ninja is supposed to turn off Slack notifications on your phone and
   set DND mode. But just in case, you may want to do one of these:*
   1. log out from Slack and Gmail on your phone, or
   1. remove Slack and Gmail from your phone, or
   1. pause the Slack and Gmail apps on your phone
   1. manually configure Slack and Gmail to send no notifications on your phone

### Re-connect

Please consider adding an agenda item after you get back to let the rest of the
team know what you've been up to!

