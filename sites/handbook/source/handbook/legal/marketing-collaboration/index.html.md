---
layout: handbook-page-toc
title: "Marketing Guide: Collaborating with GitLab Legal"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
This page is intended to provide educational resources and process guides to GitLab Marketing for recurring Legal requests.

## Brand and Trademark
 
A trademark is a word, symbol, device, or any combination used to identify and distinguish one's goods or services from others.  It is a form of Intellectual Property and it has value; so much value that using someone else’s trademarks improperly or failing to protect your own can cost companies large amounts of money.   

There are important things to consider when settling on a name and ensuring we get the most value from our mark as possible.

**Strength:** The ideal trademarks are "fanciful" or "arbitrary." Fanciful is just a nice way of saying “made up”.  This category includes marks like Kodak, Xerox, iPod, Lego, etc.; you know, those words that had absolutely no meaning before they were trademarked.   Arbitrary just means that the name and the product are not generally found together or otherwise related.  For instance, computers and apples generally don’t go together; therefore, Apple has a very strong mark.  

**Use:** Unlike patents, in order to protect a trademark – you must use it.  You can’t register it and then let it sit on the shelf until you decide what to do with it.  It must be used AND it must be used in the manner with which you said you would use it. Misusing even a great trademark can make it so common that it can get difficult and expensive to protect. 

**Market:** A trademark does not prevent everyone else from using that same word.  For example, there is Delta Airline, Delta Dental and Delta faucets.  The key distinction here is likelihood of confusion.  If you see “Delta” on a plane – you know that it is the airline company.  If you see “Delta” on a sink faucet, is there a real likelihood that you would expect frequent flier miles from it? Probably not. So two very different companies can use the same name because most people are smart enough to realize that these are very different companies.  

While you can have the same word for *different* markets, you can’t have different words for similar markets if those different words are similar to a trademarked word (back to that likelihood of confusion standard).   

**Registration:** Another thing to consider is if I should register the mark.  In some common law countries (such as US, UK, South Africa, Australia, et al), those who use the mark first get protection. (These trademarks are denoted by a  “TM”.) Admittedly, it can get difficult to determine when that first use occurred; so registration in common law countries helps to clear that date issue up.  In any non-common law country, the first to file owns the mark. To set a date certain and own the mark, marks get registered.  If a mark is registered, it gets an “®”.  Whenever you use an R for a mark, you should also state where the mark is registered. 

**Cultural Significance:**  Last but not least, cultural sensitivity is critical.  Any attempt to market globally and/or translate trademarks must be met with heightened attention.  

* Coors put its slogan, "Turn it loose," into Spanish, where it was read as "Suffer from diarrhea". 
* Clairol introduced the "Mist Stick", a curling iron, into Germany only to find out that "mist" is slang for manure. Not too many people found a use for the "manure stick". 
* In Chinese, the Kentucky Fried Chicken slogan "finger-lickin' good" came out as "eat your fingers off". 
* An American T-shirt maker in Miami printed shirts for the Spanish market which promoted the Pope's visit. Instead of "I saw the Pope" (el Papa), the shirts read "I saw the potato" (la papa). 
* In Italy, a campaign for Schweppes Tonic Water translated the name into "Schweppes Toilet Water". 
* Pepsi's "Come alive with the Pepsi Generation" translated into "Pepsi brings your ancestors back from the grave", in Chinese. 
* When Parker Pen marketed a ball-point pen in Mexico, its ads were supposed to have read, "it won't leak in your pocket and embarrass you". Instead, the company thought that the word "embarazar" (to impregnate) meant to embarrass, so the ad read: "It won't leak in your pocket and make you pregnant". 
* The Coca-Cola name in China was first read as "Ke-kou-ke- la", meaning "Bite the wax tadpole" or "female horse stuffed with wax", depending on the dialect. Coke then researched 40,000 characters to find a phonetic equivalent "ko-kou-ko- le", translating into "happiness in the mouth".

Like with any conversation, it is critical to be aware of how your words are received in different cultures. 

Note: Trademarks aren’t just words.  As mentioned above, trademarks can be symbols, as well, and this catetgory is very broad. Take the following examples -
* **Colors** Owens Corning trademarked the color pink for insulation. No other company can make insulation that color.  If you ever see insulation that is pink, you know who made it. John Deere trademarked a specific color of green for farm equipment.  McDonald’s has the Golden Arches.  
* **Sounds**  NBC trademarked the chimes that you hear during intermissions. Intel has a trademarked sound. 
* **Shapes** One of the most famous non-word trademarks is the shape of a Coke bottle.  Once you start getting into how something looks, it is called trade dress. Trade dress is a fancy term for trademarked product presentation.

### GitLab's Trademark
Marketing owns GitLab's [Brand Guidelines](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/brand-guidelines/).

### Using Third-party Trademarks
Any use of third-party trademarks should be covered by a Trademark Use Agreement. Various Marketing teams manage these agreements, requesting help from Legal team contract managers only when terms need to be negotiated. If you would like to use a 3rd party logo, please work with the appropriate Marketing team to ensure there is a trademark use agreement in place or to request such an agreement. 

- [Channel Marketing](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/channel-marketing/#how-to-ask-for-workassistance-from-channel-marketing) 
- [Partner Marketing](https://about.gitlab.com/handbook/marketing/product-marketing/partner-marketing/#how-to-engage-partner-marketing) 
- [Customer Reference Marketing](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program)

## Contests and Sweepstakes

From time to time, GitLab runs contests and sweepstakes. While these seem simple enough and may be an effective marketing tactic, there are quite a few factors to consider when planning such an event. If you are planning such an event, you will need legal approval for the proposed event. Please use the [Sweepstakes Legal Template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=sweepstakes_legal_template) housed in the Marketing Ops repo. 

The following explanations will help you fill out the form completely. This is essential, as all of the questions were developed based on legal requirements. Issues that do not answer all the questions will not be approved.

- **Type of Event.** Please check the type of event you are hosting or add an additional type if the options do not fit your event. This is an important step as the type of event is often the starting point for knowing what laws to review.
  - Survey: A survey is based on preference or individual opinion, such as a questionnaire in which there are no wrong answers.
  - Quiz: A quiz is based on knowledge and there are right and wrong answers.
  - Contest: A contest is based on skill and involves exhibiting this skill in a measurable way.

- **Hosting Country.** Because [laws vary based on locale](https://www.dlapiperintelligence.com/prizepromotions/insight/index.html?t=), it is necessary for Legal to know as much as possible about where the event will take place. (In the case of remote events, please tell us the intended audience. For example, it may be targeted to EMEA users/prospects. Legal will then review our internal guidelines to determine which country we will designate as the host country.)

- **Event Description.** Various countries have certain requirements regarding what information must be communicated in the event descriptions. While you may make small edits to your description throughout the process of developing your event, it is important for Legal to know what communication to entrants will look like so we can ensure that it complies with local law.

- **Use of Information.** You must articulate what you will do with information you obtain through the event so Legal can help you ensure that communication and/or consent is handled properly.

- **Questions.** The questions you intend to use for a survey or quiz must be submitted for review. This is mainly for informational purposes so that Legal can understand more about the event and what rules may apply. It is unlikely that we will "disallow" the questions, but we may have input for wording. The questions will also help us know what language to include in customized Terms and Conditions.

- **Prize Values.** Local law often hinges on the value of the prizes, so it is necessary to know what prizes and how many prizes will be awarded prior to creating Terms and Conditions. Legal may request a change in prizes if the value amount is over a certain threshhold and adds significant Legal requirements to event. This will vary from country to country and sometimes from state to state.

- **Awarding Prizes.** The method and criteria for winning a prize must be detailed, as this will affect what the Terms and Conditions will include.

- **Winner List.** You must provide a url for posting a winner list, as this is a requirement in nearly every jurisdiction. We take privacy seriously, so we will post first initials and last names where allowed by law. 

Once your issue description is completely filled out, Legal will review all the details in light of the applicable laws and make recommendations to tailor the event. Legal will also provide Terms and Conditions. **Depending on the locale, you may be required to obtain a translation of the Terms and Conditions to locale languages.** Legal does not provide this, so you will need to ensure that you have the ability to procure this through Marketing vendors. Some Terms and Conditions can be translated through automated services, but others may require human translation depending on the prize amounts and jurisdiction.

















