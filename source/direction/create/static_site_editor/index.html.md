---
layout: markdown_page
title: "Category Direction - Static Site Editor"
description: "Static websites are a flexible and performant option for many types of sites including blogs, documentation portals, and portfolios. The Static Site Editor aims to make editing content on these websites a delightful and intuitive experience."
canonical_path: "/direction/create/static_site_editor/"
---

- TOC
{:toc}

## Static Site Editor

|  Stage   |   Maturity  |   Content Last Reviewed   |
|  ---   |   ---   |   ---   |
| [Create](/direction/dev/index.html#create) | [Minimal](/direction/maturity/) | `2020-09-02` |

## Introduction and how you can help

This is the category direction page for the Static Site Editor in GitLab. This page belongs to the [Static Site Editor](/handbook/product/product-categories/#static-site-editor-group) group of the Create stage and is maintained by Sr. Product Manager, [Eric Schurter](https://about.gitlab.com/company/team/#ericschurter).

Established as a category in late 2019, the Static Site Editor is relatively new to GitLab. Our direction is evolving quickly as we iterate on our initial MVC and work to deliver value to as many GitLab users as possible. We'd love to hear from you, especially if you manage or collaborate on static websites.

Sharing your questions or feedback directly on [GitLab.com](https://gitlab.com/) is the best way to contribute to our category direction. Please don't hesitate to leave a comment on our [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Astatic%20site%20editor) or [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AStatic%20Site%20Editor) and, of course, we welcome community contributions. If you'd rather provide feedback privately or schedule a video call, you can email our Product Manager, ([Eric Schurter](mailto:eschurter@gitlab.com)).

## Overview

Our mission is to ensure that creating and editing content on [static websites](https://en.wikipedia.org/wiki/Static_web_page) does not require deep knowledge of any particular templating language, Markdown syntax, or git branching workflows. The Static Site Editor category is focused on delivering a familiar and intuitive content editing experience that enables efficient collaboration on static sites across all users, on any platform, regardless of their technical experience.

Static websites are a flexible and performant option for many types of sites, including blogs, documentation portals, and portfolios. These use cases necessitate the ability for content to be edited quickly, frequently, and often away from a desktop computer. For this reason, the goal is to deliver a delightful content editing experience on desktops, tablets, and mobile devices.

### Target Audience

- **[Sasha (Software Developer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer):** While collaboration on static website content should be accessible to personas with less technical expertise, developers like Sasha are the ones who have to create and configure the project in the first place. Even with her technical ability, after configuring the project, Sasha may find it easier to edit content using a more streamlined, content-focused editor instead of a code editor.
- **GitLab Team Members:** The GitLab handbook is a prime example of a static site and every member of the GitLab community should be able to actively contribute to it, whether or not they have the relevant technical knowledge. As this category reaches a lovable level of maturity, it will be possible to create and edit the content on the GitLab handbook without writing a single line of code.
- **UX Writers, Technical Writers, Copywriters:** Authors of website content should be focused on the content itself, not on remembering markdown syntax or specifics around templating markup. These contributors are at best slowed down and, at worst, entirely prevented from doing their job when the technology doesn't adapt to their workflow and technical ability.
- **Product Managers, Leadership, Stakeholders:** As companies move to a more distributed way of working, processes and documentation becomes increasingly important. Contributions to this type of content come from everyone. Stakeholders and members of leadership need a fast, reliable, and accessible way to collaborate with their team.

### Challenges to address

#### Creating, configuring, and hosting a static website

From choosing a programming language and a static site generator to configuring a deployment pipeline and managing their domain, users are required to make a series of decisions and piece together multiple services to create and deploy a static website from scratch. If they want to collaborate on content, yet another service might be added into the mix to manage content and provide an editing experience accessible by everyone. Users need a single place to create, configure, edit, and host their static web content.

This is a workflow that GitLab is well positioned to address and the Static Site Editor is one of the last remaining pieces needed to deliver this end-to-end experience. This opportunity is one that we have not fully addressed in the current iteration of the Static Site Editor, but it will be an area where we will be investing time to [validate the problem space](https://about.gitlab.com/handbook/product-development-flow/#validation-phase-2-problem-validation).

#### Editing content on an existing static page

Collaborating on static web content can be a challenging, even intimidating experience for a variety of reasons.

- Users may not be able to find the content they want to edit in a complex project structure
- Users may not be familiar with development environments or code editors and can be intimidated by the editing experience
- Users may not be familiar with Markdown and how it relates to the final HTML output which can lead to a lack of confidence in their edits being formatted correctly
- Users may not understand the git workflow and how to create branches, commit changes, and open a merge request for review
- Small, accidental errors in syntax can cause builds to fail, making users feel like the risk of introducing small, accidental syntax errors outweighs the benefits of them contributing directly

To get around these problems, developers have to decide whether they are either going to force their contributors to learn the technical skills required to edit content or integrate with a headless CMS or other abstraction of the data model. The former can be prohibitive to collaboration and the latter can be costly and complicated to maintain.

#### Creating new content on a static website

Workflows that require creating, moving, or deleting content on a static website suffer from the same challenges as editing, but introduce additional complexities, especially for users who aren't familiar with the project's information architecture and directory structure. Users can struggle to:

- Know which layout template to use for a new page
- Understand where to put new pages
- Understand where to put images and other assets
- Manage redirects and update links to existing content that has been moved or deleted
- Create content meant to be re-used across multiple pages on the website

Similar workarounds are in place for these workflows, but often rely on either learned behavior or managing content in only the most advanced, fully-featured content management systems.

#### Publishing quality content to a static website

Once the edits have been made, the content still has to get built and deployed before it is available on the web. This can be a time-consuming process and one that can fail in many places along the way.

- Users want to feel confident that their changes will render correctly before starting the deployment process
- Users want to be notified if anything goes wrong along the way
- Users want to be able to submit proposed changes for review before publishing to the website

The Git-based workflow is perfectly suited to handle these challenges, but it does require a significant amount of prior knowledge around version control and local development environments. A proper CMS can abstract all of this away from the user, but often does so at the expense of flexibility, preventing those who do know the inner-workings of Git from performing advanced tasks.

## Where we are Headed

Contributors want to limit context switching between the site itself and the underlying repository structure, so the editing experience for content hosted on a static site should be accessible and available in the context of the webpage itself.

The editing experience will be intuitive and scalable for those who do not know, or do not wish to write, Markdown. To compliment a more "what you see is what you get" (WYSIWYG) editing experience, we'll make it possible to preview the edits live and in real-time on the site using the page's custom styles and layout.

At the end of the journey, publishing content to the site will be improved by abstracting and streamlining a lot of the process related to creating branches, committing changes, and creating merge requests while maintaining the power of a versioned, distributed, git-based backend.


### What's Next & Why

Now that the Static Site Editor is available to everyone in the form of a [Middleman-based project template](https://gitlab.com/gitlab-org/project-templates/static-site-editor-middleman) and fully [integrated into the GitLab Handbook](https://gitlab.com/groups/gitlab-org/-/epics/2786), we are focused on:

1. [Improving the usability and overall experience of the WYSIWYG editor](https://gitlab.com/groups/gitlab-org/-/epics/3217)
1. [Making the editor more configurable to better support real-world projects](https://gitlab.com/groups/gitlab-org/-/epics/2797)
1. [Handling more complex publishing workflows](https://gitlab.com/groups/gitlab-org/-/epics/4310)
1. [Making it easier to access the static site editor](https://gitlab.com/groups/gitlab-org/-/epics/4311)
1. [Extending the reach of the static site editor](https://gitlab.com/groups/gitlab-org/-/epics/4312)

#### Static Site Editor UX

- A page's YAML front matter metadata is not meant to be rendered with the content. In GitLab 13.1, we [hid the front matter from the WYSIWYG editor](https://gitlab.com/gitlab-org/gitlab/-/issues/216834). We're currently working on [a more user-friendly editing experience for this metadata](https://gitlab.com/gitlab-org/gitlab/-/issues/209977).
- The WYSIWYG editor renders [thumbnail previews of existing images](https://gitlab.com/gitlab-org/gitlab/-/issues/216640) and allows a contributor to insert existing images already hosted in the project. Next, we'll be working on the ability to [upload new images](https://gitlab.com/gitlab-org/gitlab/-/issues/218529) directly from the Static Site Editor.

#### Configuring the Static Site Editor

- Due to the round-trip Markdown-to-HTML conversion, the WYSIWYG editor requires strict adherence to a Markdown syntax specification. However, Markdown itself is designed to have some flexibility in syntax preferences. For example, you can use `*`, `-`, or even `+` to initiate an unordered list. The Static Site Editor needs to be [configurable for users' preferences](https://gitlab.com/gitlab-org/gitlab/-/issues/244483) to avoid needless re-formatting of content that doesn't conform to the expected Markdown syntax.
- Configuring Markdown syntax preferences isn't the only customization that users need to make the static site editor fit into their projects. Another key area is the [configuration of an asset path (or multiple paths) to aide the editor when uploading and rendering assets](https://gitlab.com/gitlab-org/gitlab/-/issues/216641) in the WYSIWYG mode.
- A page's YAML front matter is important, but it's syntax is strict and a simple formatting error or typo can lead to a failed build. Editable front matter in the WYSIWYG editor is only the beginning. A date picker could be used instead of a text field which would eliminate the guesswork around date formats. Or a list of available templates could populate a select box instead of requiring a user to remember what options are available. We want to empower developers to configure how the front matter fields are edited, encouraging more accurate and more reliable edits.

#### Publishing workflows

- Version control best practices encourage providing clearly-named merge requests that add context to your contribution. We're working to extend the editor's publishing workflow to allow for [custom merge request titles and descriptions](https://gitlab.com/gitlab-org/gitlab/-/issues/216861) before submitting the merge request.
- The Static Site Editor is currently limited to editing a single page at a time. We're working on adding the ability to [edit multiple pages in a single merge request](https://gitlab.com/gitlab-org/gitlab/-/issues/244485) and to [contribute additional edits to an existing merge request](https://gitlab.com/gitlab-org/gitlab/-/issues/216863) after the initial submission.

#### Editor discoverability

- The Static Site Editor is accessible from a URL that is easily incorporated into your site or bookmarked for easy access. However, once you're in the GitLab platform, there is no clear way to access the editor again. We'd like to provide a way to list all the pages in a given project that can be edited in the Static Site Editor in order to provide an alternate entry point into the editor.
- In order to enable a publishing workflow that supports editing multiple pages at once, we need the ability to quickly and easily [navigate between pages from the Static Site Editor](https://gitlab.com/groups/gitlab-org/-/epics/2803).
- We don't want the Static Site Editor to be limited to editing existing content. We're looking to support [creating new pages](https://gitlab.com/groups/gitlab-org/-/epics/2800), as well as [moving](https://gitlab.com/groups/gitlab-org/-/epics/2802), renaming, and deleting pages from the project.

#### Extensibility

- We want everyone to be able to use the Static Site Editor on their projects, so we are looking to [support additional static site generators](https://gitlab.com/groups/gitlab-org/-/epics/4309) like [Hugo](https://gohugo.io/), [Jekyll](https://jekyllrb.com/), [Next.js](https://nextjs.org/), and [Gatsby](https://www.gatsbyjs.org/).

### What is Not Planned Right Now

Right now, we are not planning on investing any time in developing a new framework for generating static sites or forking an existing project to extend its functionality. We are also not choosing a single static site generator on which to build our product. Instead, our goal is to provide a solution that works across many, if not all, of the most popular platforms like [Middleman](https://middlemanapp.com/), [Jekyll](https://jekyllrb.com/), [Hugo](https://gohugo.io/), and [Gatsby](https://www.gatsbyjs.org/).

The Static Site Editor group is not working toward a solution that allows users to create static sites from scratch without writing code. The initial setup of a static site will, for now, remain something that requires at least some technical knowledge and configuration.

We are also not planning to build a way to visually edit relational databases, API, or other dynamic content. As the group name suggests, our focus is on bringing a user-friendly interface for editing static content.

While we hope to provide visual formatting tools for a more familiar text editing experience, the underlying text markup language will remain Markdown. We will not support writing content in alternative markup languages like LaTeX, Org-mode, or Asciidoc.

### Maturity Plan

This category is currently at the Minimal maturity level (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/product-categories/maturity/#legend)).

The [Category Strategy epic](https://gitlab.com/groups/gitlab-org/-/epics/2688) is where we're planning the work necessary to reach Viable maturity. This includes the introduction of a rich text WYSIWYG editor which was made available in 13.0, as well as a more flexible approach to making multiple edits across pages and an initial approach to handling non-Markdown content on a page. We plan on reaching the Viable maturity level by September 22, 2020 after successfully integrating it into the GitLab handbook.

### User success metrics

It doesn't matter as much to us whether a merge request gets deployed and while we hope everyone gets a chance to try out the Static Site Editor, simply opening up the editor doesn't mean you've contributed any content. Our primary [performance indicator](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#createstatic-site-editor---gmau---mau-that-committed-via-the-sse) revolves entirely around a commit action, which is an indication of a successful contribution.

We want to encourage more people to commit more often. This would mean that users of services outside of GitLab to edit content can be brought into the platform to streamline their collaboration. It also means that existing users will be exposed to more features GitLab has to offer across the many stages of the DevOps workflow.

### Why is this important?

A delightful, accessible editing experience is the last piece of the puzzle for enabling end-to-end management of static sites within GitLab. The end goal is to streamline collaboration with engineers by bringing users into GitLab rather than spreading the work across multiple products, resulting in both cost and time efficiencies for our users.

## Competitive Landscape

Products that directionally inspire our vision for the Static Site Editor

- [Netlify CMS](https://www.netlifycms.org/)
- [Forestry.io](https://forestry.io/)
- [TinaCMS](https://tinacms.org/)
- [Stackbit](https://www.stackbit.com/)

<!-- ## Analyst Landscape -->
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!-- ### Top user issue(s) -->
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

<!-- ### Top internal customer issue(s) -->
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/values/#dogfooding)
the product.-->

<!-- ### Top Strategy Item(s) -->
<!-- What's the most important thing to move your strategy forward?-->
