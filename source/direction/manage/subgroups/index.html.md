---
layout: markdown_page
title: "Category Direction - Subgroups"
description: "Groups are a fundamental building block (a small primitive) in GitLab for project organization and managing access to these resources at scale. Learn more!"
canonical_path: "/direction/manage/subgroups/"
---

- TOC
{:toc}

| **Stage** | **Maturity** | **Content Last Reviewed** |
| --- | --- | --- |
| [Manage](/direction/manage/) | [Complete](/direction/maturity/) | `2020-08-03` |

## Introduction and how you can help
Thanks for visiting this category page on Subgroups in GitLab.  This page is being actively maintained by [Melissa Ushakov](https://about.gitlab.com/company/team/#mushakov). This vision and direction is a work in progress and sharing your feedback directly on issues and epics on GitLab.com is the best way to contribute. 

## Overview

Groups are a fundamental building block (a [small primitive](https://about.gitlab.com/handbook/product/product-principles/#prefer-small-primitives)) in GitLab that serve to:  
- Define workspace boundaries, particularly on GitLab.com, where a top level group frequently represents an organization
- Group users to manage authorization at scale
- Organize related projects together
- House features that cover multiple projects like epics, contribution analytics, and the security dashboard

### Our mission

In 2020, our goal is to make improvements by extending Subgroups to help enterprise organizations thrive on GitLab.com. We will accomplish that by iterating on existing constructs and streamlining workflows to guide users to intended usage patterns.  

#### Challenges

Historically, enterprise customers have gravitated toward self-managed GitLab as their favored solution. With the proliferation of cloud services, enterprises are looking for options to use GitLab with managed infrastructure. We need to provide a SaaS platform that can safely and securely house our customers while reducing the load and cost of having to manage and configure their own infrastructure. 

#### Opportunities

Iterating on subgroups will allow us to solve a number of problems with the current SaaS experience:
* **Increase isolation** - We need mechanisms to isolate an organization from the rest of GitLab.com so that we can meet enterprise regulatory and security requirements. Creating clear workspace boundaries in GitLab.com  will prevent users from unintentionally exposing users, projects, or other sensitive information to the rest of the GitLab.com instance. A top-level group should feel like a self-contained space where an enterprise can work independently of the rest of Gitlab.com. Users in a group owned by an enterprise should be able to complete all their day to day activities without leaving their organization's group. 
* **Additional control** - On GitLab.com, user accounts and all associated details are owned by a user, not the groups they belong to. This is problematic for group administrators since they can't ensure the level of data integrity necessary for their audit and regulatory needs. We will introduce ways for users to set group-specific profile information that they can allow group owners to manage. 
* **Flexible hierarchies** - We need to offer additional configuration options to allow enterpises to represent different [types of organizationas](https://creately.com/blog/diagrams/types-of-organizational-charts/) using subgroups.

## Target audience and experience

Groups are used by all GitLab users, no matter what role. However, heavier users of Groups in terms of organization and management of projects and users consist of:

* **Group Owners**
* **System Admins**
* **Team Leaders, Directors, Managers**
* **Individual contributors** who predominantly work with GitLab's project management features

## What's Next and Why

The issues below have been prioritized to help solve the problems outlined in the Opportunities section.

* Increase isolation 
  * [Prevent forking outside of a group](https://gitlab.com/gitlab-org/gitlab/-/issues/216987) - Some organizations prefer to have their group be the only place that a user's able to interact with their projects. By keeping forks within the group, they're able to assert control over them and audit activity associated with the groups repositories.
  * [Improve project creation UX for users in a groups](https://gitlab.com/gitlab-org/gitlab/-/issues/216608) -  For users in an enterprise, the default workflows should encourage work to stay within that organization's group. We should offer sensible defaults and warn users if they create projects outside of their enterprises group.
* Additional control 
  * Allow group owners to manage user details through [personas](https://gitlab.com/gitlab-org/gitlab/-/issues/218631) - A user on .com has a single account of which they retain 100% ownership. Personas will allow us to develop a shared ownership model to give end users the ability to participate in multiple groups with a single account and administrators the control they need.
  * [Credential management for Gitlab.com groups](https://gitlab.com/groups/gitlab-org/-/epics/4123) - Group administrators in Gitlab.com want to take advantage of the [credential inventory](https://docs.gitlab.com/ee/user/admin_area/credentials_inventory.html#credentials-inventory-ultimate-only) and [credential management APIs](https://gitlab.com/gitlab-org/gitlab/-/issues/227264) which are only available on self-managed instances and [group-managed accounts](https://docs.gitlab.com/ee/user/group/saml_sso/group_managed_accounts.html#group-managed-accounts-premium) on gitlab.com. In order to accomplish this, we will give users the ability to create group specific PATs and SSH keys. Group administrators will then be able to see all PATs and SSH keys that include their group.   
* Flexible hierarchies
  * [Support for "secret" projects when using SSO](https://gitlab.com/gitlab-org/gitlab/-/issues/220203) - If an organization is using SSO, users are automatically added to the top-level group as Guest, which gives them access to all subgroups. We'll introduce configuration options which will allow users to be added to only the groups they need access to. 

## What is Not Planned Right Now

* [Optionally disable inheritance](https://gitlab.com/gitlab-org/gitlab/-/issues/33534)- Disabling inheritance was requested as a way to support secret projects. We explored this implementation method and found that there are many side effects to changing gorup inheritance. We have chosed to solve this problem by allowing users to [use SSO in a group and be selectively added to subgroups](https://gitlab.com/gitlab-org/gitlab/-/issues/220203).
* [Group managed accounts](https://docs.gitlab.com/ee/user/group/saml_sso/group_managed_accounts.html#group-managed-accounts-premium) - While we previously implemented group managed accounts as a solution for increasing isolation in Gitlab.com, we're not planning to continue in this direction. We received feedback from our end users and community that we should pivot to an [identity model](https://gitlab.com/gitlab-org/gitlab/-/issues/218631)) that does not require users to maintain separate accounts.

## Maturity

This category is currently **Complete**. The next step in our maturity plan is achieving a **Lovable** state. 
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.
* Note that a `Complete` state does not mean a category is "finished" and is no longer a priority. Even categories that are considered `Lovable` require continued investment.
<!-- ## Top user issue(s) -->

## Top internal customer issue(s)

🚨 GitLab Support Engineering: [Isolation & Control Requirements](https://gitlab.com/groups/gitlab-org/-/epics/2444)
